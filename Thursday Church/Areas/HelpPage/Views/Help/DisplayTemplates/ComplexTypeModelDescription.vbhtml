@Imports Thursday_Church.Areas.HelpPage.ModelDescriptions
@ModelType ComplexTypeModelDescription
@Html.DisplayFor(Function(m) Model.Properties, "Parameters")
